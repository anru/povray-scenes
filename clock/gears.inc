#include "transforms.inc"

/* screws */
#macro screw_head(p_diameter, p_thickness)
union {
  /* head */
  difference {
    cone { <0, 0, -p_thickness/2>, p_diameter, <0, 0, p_thickness/2>, p_diameter/2 * 0.8}
    box { <-p_diameter/2 * 0.1, -p_diameter/2 * 1.2, -p_thickness>, <p_diameter/2 * 0.1, p_diameter/2 * 1.2, 0> }
    
  }
  texture { T_Axis_Texture }
}
#end

#declare RotS = seed(9187);
#macro screw(p_diameter, p_thickness, p_screw_length)
union {
  /* head */
  difference {
    cone { <0, 0, -p_thickness/2>, p_diameter/2, <0, 0, p_thickness/2>, p_diameter/2 * 0.6}
    box { <-p_diameter/2 * 0.1, -p_diameter/2 * 1.2, -p_thickness>, <p_diameter/2 * 0.1, p_diameter/2 * 1.2, 0> }
    
  }
  /* screw */
  cylinder { <0, 0, 0>, <0, 0, p_screw_length*0.9>,  p_diameter/2 * 0.55 }
  cone { <0, 0, p_screw_length*0.9>, p_diameter/2 * 0.55, <0, 0, p_screw_length>, 0 }
  #local rs=.08;
  #local ns=19;
  #local is=1;
  #while (is <= ns)
    torus{ p_diameter/2 * 0.53, (p_diameter/4)*.1 scale <1, 0.8, 1> rotate <90, -5, 0> translate (.2 + (rs * 1.5 +.01)*is)*z}
   #local is = is + 1;
  #end
  rotate z*100*rand(RotS)
  texture { T_Axis_Texture }
}
#end

/* gears */
#macro gear_0(p_diameter_outer, p_diameter_inner, p_thickness, p_spike_count)
union {
  // gear
  difference {
    cylinder {<0, 0, -p_thickness/2>, <0, 0, p_thickness/2>, p_diameter_outer}
    cylinder {<0, 0, -p_thickness/2 - 1>, <0, 0, p_thickness/2 + 1>, p_diameter_inner}
    texture { T_Gear_Texture }
  }
  // part at center
  cylinder {<0, 0, -p_thickness/2>, <0, 0, p_thickness/2>, p_diameter_outer * 0.3 texture { T_Gear_Texture }}
  // spikes
  #local spike_distance = 360 / p_spike_count;
  #local spike_count = 0;
  #local spike_width = (p_diameter_outer - p_diameter_inner)/2;
  #while (spike_count <= p_spike_count)
    box {
      <-spike_width, p_diameter_outer * 0.15, -p_thickness/2>, <spike_width, p_diameter_inner + spike_width, p_thickness/2>
      rotate <0, 0, spike_distance * spike_count>
      texture { T_Gear_Texture }
    }
    #local spike_count = spike_count + 1;
  #end
}
  // 
#end

#macro gear_1(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count)
union {
  gear_0(p_diameter_outer, p_diameter_inner, p_thickness, p_spike_count)
  // axis
  cylinder {
    <0, 0, -(p_axis_length * p_axis_1) + 0.1 * p_axis_length>,
    <0, 0, p_axis_length * p_axis_2 - 0.1 * p_axis_length>,
    p_axis_diameter
    texture { T_Axis_Texture }
  }
  // axis heads
  cylinder {
    <0, 0, -p_axis_length * p_axis_1>,
    <0, 0, p_axis_length * p_axis_2>,
    p_axis_diameter/2
    texture { T_Axis_Texture }
  }
}
  // 
#end

#macro gear_2(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count, p_teeth_count)
union {
  gear_1(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count)
  //
  #local teeth_length = 0.3 * (p_diameter_outer - p_diameter_inner);
  #local total_teeth_count = p_teeth_count;
  #local teeth_count = 0;
  #local teeth_width = (p_diameter_outer * 3.1415 * 2) / (4 * p_teeth_count);
  #local teeth_distance = 360 / total_teeth_count;
  #while (teeth_count < total_teeth_count)
    union {
      prism {
	linear_sweep
	linear_spline
	-p_thickness/2,
	p_thickness/2,
	5,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>,
	<-teeth_width * 0.8, p_diameter_outer + teeth_length>,
	<teeth_width * 0.8, p_diameter_outer + teeth_length>,
	<teeth_width * 1.5, p_diameter_outer * 0.98>,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>
	rotate x*90
      }
      cylinder {
	<0, p_diameter_outer + teeth_length, -p_thickness/2>, <0, p_diameter_outer + teeth_length, p_thickness/2>, teeth_width * 0.79
      }
      rotate <0, 0, teeth_distance * teeth_count>
      texture { T_Gear_Texture }
    }
    #local teeth_count = teeth_count + 1;
  #end
}
#end

#macro gear_3(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count, p_teeth_count, p_secondary_gear_height)
union {
  gear_2(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count, p_teeth_count)
  // part at center
  cylinder {<0, 0, -p_thickness/2 - p_secondary_gear_height>, <0, 0, p_thickness/2 - p_secondary_gear_height>, p_diameter_outer * 0.2 texture { T_Gear_Texture } }
  #local total_teeth_count = p_teeth_count/8;
  #local teeth_count = 0;
  #local teeth_width = (p_diameter_outer * 3.1415 * 2) / (4 * total_teeth_count);
  #local teeth_distance = 360 / total_teeth_count;
  #while (teeth_count < total_teeth_count)
    union {
      cylinder {
	<0, p_diameter_outer * 0.15, -p_thickness/2 - p_secondary_gear_height>, <0, p_diameter_outer * 0.15, p_thickness/2>, p_diameter_outer * 0.01
      }
      sphere {<0, p_diameter_outer * 0.15, -p_thickness/2 - p_secondary_gear_height>, p_diameter_outer * 0.01}
      rotate <0, 0, teeth_distance * teeth_count>
      texture { T_Axis_Texture }
    }
    #local teeth_count = teeth_count + 1;
  #end

}
#end

// no axis, full metal gear
#macro gear_4(p_diameter_outer, p_diameter_inner, p_thickness, p_teeth_count)
union {
  // gear
  difference {
    cylinder { <0, 0, -p_thickness/2>, <0, 0, p_thickness/2>, p_diameter_outer }
    cylinder { <0, 0, -p_thickness>, <0, 0, p_thickness>, p_diameter_outer * 0.1 }
    texture { T_Gear_Texture }
  }
  //
  #local teeth_length = 0.3 * (p_diameter_outer - p_diameter_inner);
  #local total_teeth_count = p_teeth_count;
  #local teeth_count = 0;
  #local teeth_width = (p_diameter_outer * 3.1415 * 2) / (4 * p_teeth_count);
  #local teeth_distance = 360 / total_teeth_count;
  #while (teeth_count < total_teeth_count)
    union {
      prism {
	linear_sweep
	linear_spline
	-p_thickness/2,
	p_thickness/2,
	5,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>,
	<-teeth_width * 0.8, p_diameter_outer + teeth_length>,
	<teeth_width * 0.8, p_diameter_outer + teeth_length>,
	<teeth_width * 1.5, p_diameter_outer * 0.98>,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>
	rotate x*90
	rotate z*-90
      }

      cylinder {
	<p_diameter_outer + teeth_length, 0, -p_thickness/2>, <p_diameter_outer + teeth_length, 0, p_thickness/2>, teeth_width * 0.79
      }

      /*
      cylinder {
	<0, p_diameter_outer + teeth_length, -p_thickness/2>, <0, p_diameter_outer + teeth_length, p_thickness/2>, teeth_width
      }*/
      rotate <0, 0, teeth_distance * teeth_count>
      texture { T_Gear_Texture }
    }
    #local teeth_count = teeth_count + 1;
  #end
}
#end

#macro gear_5(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count, p_teeth_count, p_secondary_gear_height)
union {
  gear_2(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count, p_teeth_count)
  // enhanced part at center
  object {
    gear_4(p_diameter_outer * 0.15, p_diameter_outer * 0.05, p_secondary_gear_height, p_teeth_count/5)
    translate <0, 0, -(p_secondary_gear_height/2)>
  }
  texture { T_Gear_Texture }
}
#end

/* special gears */

/* barrel gear */
#macro gear_barrel(p_diameter_outer, p_diameter_inner, p_thickness, p_teeth_count, p_spring_chamber_thickness)
union {
  gear_4(p_diameter_outer, p_diameter_inner, p_thickness, p_teeth_count)
  cylinder {
    <0, 0, 0>,
    <0, 0, p_spring_chamber_thickness>,
      p_diameter_outer * 0.9
    texture { T_Gear_Texture }
  }
  // axis
  cylinder {
    <0, 0, -p_thickness>,
    <0, 0, p_spring_chamber_thickness * 1.2>,
    p_thickness * 4
    texture { T_Axis_Texture }
  }
  // axis head only on back
  cylinder {
    <0, 0, 0>,
    <0, 0, p_spring_chamber_thickness * 1.4>,
    p_thickness / 8
    texture { T_Axis_Texture }
  }
}
#end

#macro gear_escape_wheel(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count, p_teeth_count, p_secondary_gear_height)
union {
  gear_1(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count)
  //
  #local teeth_length = (p_diameter_outer - p_diameter_inner) * 2;
  #local total_teeth_count = p_teeth_count;
  #local teeth_count = 0;
  #local teeth_width = (p_diameter_outer * 3.1415 * 2) / (8 * p_teeth_count);
  #local teeth_distance = 360 / total_teeth_count;
  #while (teeth_count < total_teeth_count)
    union {
      prism {
	linear_sweep
	linear_spline
	-p_thickness/2,
	p_thickness/2,
	5,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>,
	<-teeth_width * 0.8 + teeth_length * 0.9, p_diameter_outer + teeth_length>,
	<teeth_width * 0.8 + teeth_length * 0.9, p_diameter_outer + teeth_length>,
	<teeth_width * 1.5, p_diameter_outer * 0.98>,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>
	rotate x*-90
      }
      rotate <0, 0, teeth_distance * teeth_count>
      #local teeth_count = teeth_count + 1;
    }
  #end
  // enhanced part at center
  object {
    gear_4(p_diameter_outer * 0.25, p_diameter_outer * 0.15, p_secondary_gear_height, p_teeth_count)
    translate <0, 0, -(p_secondary_gear_height/2)>
    //gear_4(p_diameter_outer * 0.15, p_diameter_outer * 0.05, p_thickness * 10, p_teeth_count/5)
    //translate <0, 0, -(p_thickness * 5)>
  }
  texture { T_Gear_Texture }
}
#end

/* Flat quad with four points */
#macro quad(p1,p2,p3,p4)
    triangle{p1,p2,p3}
    triangle{p1,p3,p4}
#end

/* balance wheel */
#macro gear_balance(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count)
#local RBALANCE = seed(8172);
union {
  gear_0(p_diameter_outer, p_diameter_inner, 2*p_thickness, p_spike_count)
  // axis
  cylinder {
    <0, 0, -(p_axis_length * p_axis_1) + 0.1 * p_axis_length>,
    <0, 0, p_axis_length * p_axis_2 - 0.1 * p_axis_length>,
    p_axis_diameter
    texture { T_Axis_Texture }
  }
  // axis heads
  cylinder {
    <0, 0, -p_axis_length * p_axis_1>,
    <0, 0, p_axis_length * p_axis_2>,
    p_axis_diameter/2
    texture { T_Axis_Texture }
  }
  #local total_weight_count = 5;
  #local weight_count = 0;
  #local weight_distance = 360 / total_weight_count;
  union {
  #while (weight_count < total_weight_count)
    union {
      cone {
	<p_diameter_inner * 0.95, 0, 0>, 0
	<p_diameter_outer, 0, 0>, p_thickness/2
      }
      cylinder {
	<p_diameter_outer, 0, 0>,
	<p_diameter_outer * 1.1, 0, 0>,
	p_thickness
      }
      rotate <0, 0, weight_distance * weight_count * rand(RBALANCE)>
    }
    #local weight_count = weight_count + 1;
  #end
    texture { T_Gear_Texture }
  }
#if (1)
  /* Archimedes */
  union {
    #local e_a = 0.045;
    #local e_t = 1000;
    #local pos_x = 0;
    #local pos_y = 0;
    #local prev_pos_x = 0;
    #local prev_pos_y = 0;
    #while (e_t < 6000)
      #local pos_x = e_a*radians(e_t)*cos(radians(e_t));
      #local pos_y = e_a*radians(e_t)*sin(radians(e_t));
      #if (e_t > 1)
	cylinder { <prev_pos_x, prev_pos_y, 0>, <pos_x, pos_y, 0>, 0.05 }
      #end
      #local prev_pos_x = pos_x;
      #local prev_pos_y = pos_y;
      #local e_t = e_t + 10;
    #end
    #declare last_spring_pos = <pos_x, pos_y, -(p_axis_length * p_axis_1)*0.3>;
    #debug "Define last spring pos\n"
    translate z*-(p_axis_length * p_axis_1)*0.3
    texture { T_Gear_Texture }
  }
#end
#if (0)
  #local direct = 0.00001;
  #local sum1 = frame_number*direct;
  union {
    #local e_a = 0.0001;
    #local e_b = 0.037 + sum1;
    #local e_t = 15000;
    #local pos_x = 0;
    #local pos_y = 0;
    #local prev_pos_x = 0;
    #local prev_pos_y = 0;
    #debug "here1\n"
    #while (e_t < 17500)
      #local pos_x = e_a*exp(e_b*radians(e_t))*cos(radians(e_t));
      #local pos_y = e_a*exp(e_b*radians(e_t))*sin(radians(e_t));
      #if (e_t > 1)
	//sphere {<0, 0, 0>, 0.05 pigment { Red } translate <pos_x, pos_y, -p_thickness/2>}
	cylinder { <prev_pos_x, prev_pos_y, 0>, <pos_x, pos_y, 0>, 0.05 }
      #end
      #local prev_pos_x = pos_x;
      #local prev_pos_y = pos_y;
      #local e_t = e_t + 10;
    #end
    texture { T_Gear_Texture }
    //pigment { Red }
  }
#end
}
  // 
#end

#include "regulator.inc"
#macro balance_wheel_supports(p_diameter, p_thickness)
union {
  union {
    /*
    difference {
      cylinder {<0, 0, -p_thickness/2>, <0, 0, p_thickness/2>, p_diameter/2}
      cylinder {<0, 0, -p_thickness>, <0, 0, p_thickness>, p_diameter/2 * 0.8}
    }
    box {<-p_diameter, -p_diameter/2*0.2, -p_thickness/2>, <-p_diameter/2, 0, p_thickness/2> rotate z*20}
    box {<p_diameter/2, -p_diameter/2*0.08, -p_thickness/2>, <p_diameter/2*3, p_diameter/2*0.08, p_thickness/2>}
  */
    object {regulator rotate x*-90 scale <(p_diameter/2 + p_diameter/2*2)/regulator_MAX_X, (p_diameter/2 + p_diameter/2*2)/regulator_MAX_X, 0.2>}
    texture { T_Axis_Texture }
  }
  difference {
    cylinder {<0, 0, -2*p_thickness>, <0, 0, 2*p_thickness>, p_diameter/2 * 0.8}
    cone {<0, 0, -2*p_thickness*1.2>, p_diameter/2*0.3 <0, 0, 2*p_thickness>, p_diameter/2 * 0.1}
    union {
      cylinder {<0, 0, -2*p_thickness*1.2>, <0, 0, 2*p_thickness*1.2>, p_diameter/2*0.1 translate x*-p_diameter/4}
      cone {<0, 0, -2*p_thickness*1.2>, p_diameter/2*0.2, <0, 0, -2*p_thickness*0.8>, p_diameter/2*0.1 translate x*-p_diameter/4}
      cylinder {<0, 0, -2*p_thickness*1.2>, <0, 0, 2*p_thickness*1.2>, p_diameter/2*0.1 translate x*p_diameter/4}
      cone {<0, 0, -2*p_thickness*1.2>, p_diameter/2*0.2, <0, 0, -2*p_thickness*0.8>, p_diameter/2*0.1 translate x*p_diameter/4}
      rotate z*30
    }
    texture { T_Body_Texture }
  }
  
  /* regulator to balance spring */
  cylinder { <0, 0, 3>, <0, 0, -2*p_thickness>,  p_diameter/2*0.05 translate <-4.8, 0, 0> rotate z*19 texture { T_Gear_Texture } }

  union {
    object { screw(p_diameter*0.18, p_thickness*0.1, 1*p_thickness*1.2) rotate z*100*rand(RotS) translate x*-p_diameter/4 translate z*-2*p_thickness*0.9 }
    object { screw(p_diameter*0.18, p_thickness*0.1, 1*p_thickness*1.2) rotate z*100*rand(RotS) translate x*p_diameter/4 translate z*-2*p_thickness*0.9 } 
    rotate z*30
    texture { T_Axis_Texture }
  }
}
#end

/* pinion */
#include "pinion.inc"
#macro gear_pinion(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter)
union {
  // axis
  cylinder {
    <0, 0, -(p_axis_length * p_axis_1) + 0.1 * p_axis_length>,
    <0, 0, p_axis_length * p_axis_2 - 0.1 * p_axis_length>,
    p_axis_diameter
    texture { T_Axis_Texture }
  }
  // axis heads
  cylinder {
    <0, 0, -p_axis_length * p_axis_1>,
    <0, 0, p_axis_length * p_axis_2>,
    p_axis_diameter/2
    texture { T_Axis_Texture }
  }
  // pinion
  object { pinion rotate x*-90 scale p_diameter_outer/pinion_MAX_Y texture { T_Gear_Texture } }
}
#end

#macro gear_winding_click(p_diameter_outer, p_diameter_inner, p_thickness, p_teeth_count)
union {
  // gear
  difference {
    cylinder {<0, 0, -p_thickness/2>, <0, 0, p_thickness/2>, p_diameter_outer}
    cylinder {<0, 0, -p_thickness/2 - 1>, <0, 0, p_thickness/2 + 1>, p_diameter_inner}
    texture { T_Gear_Texture }
  }
  //gear_1(p_diameter_outer, p_diameter_inner, p_thickness, p_axis_length, p_axis_1, p_axis_2, p_axis_diameter, p_spike_count)
  //
  #local teeth_length = p_diameter_outer * 0.3;
  #local total_teeth_count = p_teeth_count;
  #local teeth_count = 0;
  #local teeth_width = (p_diameter_outer * 3.1415 * 2) / (8 * p_teeth_count);
  #local teeth_distance = 360 / total_teeth_count;
  #while (teeth_count < total_teeth_count)
    union {
      prism {
	linear_sweep
	linear_spline
	-p_thickness/2,
	p_thickness/2,
	5,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>,
	<-teeth_width * 0.8 + teeth_length * 0.9, p_diameter_outer + teeth_length>,
	<teeth_width * 1.8 + teeth_length * 0.9, p_diameter_outer + teeth_length>,
	<teeth_width * 4.5, p_diameter_outer * 0.98>,
	<-teeth_width * 1.5, p_diameter_outer * 0.98>
	rotate x*-90
      }
      rotate <0, 0, teeth_distance * teeth_count>
      #local teeth_count = teeth_count + 1;
    }
  #end
  texture { T_Gear_Texture }
}
#end

#macro gear_cylider(p_diameter, p_length, p_teeth_count)
union {
  difference {
    cylinder {<0, 0, -p_length>, <0, 0, 0>, p_diameter}
    cylinder {<0, 0, -p_length - 0.1>, <0, 0, 0>, p_diameter * 0.1}
    #local teeth_distance = 360 / p_teeth_count;
    #local teeth_count = 0;
    #local teeth_width = (p_diameter * 3.1415 * 2) / (2 * p_teeth_count);
    #while (teeth_count <= p_teeth_count/2)
      cylinder {<0, -p_diameter, 0>, <0, p_diameter, 0>, teeth_width scale <0.8, 1, 1> rotate <0, 0, teeth_distance * teeth_count> translate <0, 0, -p_length>}
      #local teeth_count = teeth_count + 1;
    #end
  }
  texture { T_Gear_Texture }
}
#end
/* gear tests */
#if (0)
object {
  gear_1(3, 2.7, 0.4, 3, 0.2, 3)
  //texture { pigment { color rgbt <0.6, 0.6, 0.5, 0.1> }}
  //texture { T_Brass_5C }
  translate <-5, 0, 0>
}

object {
  gear_2(3, 2.7, 0.2, 3, 0.2, 4, 90)
  rotate <0, 45, 0>
  translate <5, 0, 0>  
}

object {
  gear_3(3, 2.7, 0.1, 3, 0.2, 3, 120, 1)
  rotate <0, 45, 0>
  translate <0, -5, 0>
}

object {
  gear_4(3, 2.7, 0.1, 120)
  rotate <0, -45, 0>
  translate <0, 2, 0>
}

object {
  gear_5(3, 2.7, 0.1, 3, 0.2, 3, 120, 1)
  rotate <0, 45, 0>
}
#end
