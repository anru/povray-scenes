#include "colors.inc"
#include "stones.inc"
#include "textures.inc"
#include "glass.inc"
#include "functions.inc"
#include "metals.inc"
#include "golds.inc"
#include "woods.inc"

#declare use_fancy_textures = 1;
#include "clock_textures.inc"
#include "gears.inc"

global_settings {
  assumed_gamma 2
}


#local use_area_light = 1;
#if (use_area_light = 1)
  light_source {
    <-400, 250, -50>/5, <1,.8,.4>
    fade_distance 15*6 fade_power 2
    area_light x*3, y*3, 12, 12 circular orient adaptive 0
  }
  light_source {
    <150, 200, 400>/5, <.3,.8,1>
    fade_distance 5*5 fade_power 2
    area_light x*3, y*3, 12, 12 circular orient adaptive 0
  }
#else
  light_source { <1000, 1000, -1000> White *0.3  }
  light_source { <-1000,1000,-1000> White * 0.3 shadowless }
  light_source { <0, 0,-1000> White*0.3 shadowless }
#end

#declare use_hdr = 0;
#if (use_hdr = 1)
  sky_sphere {
    pigment {
      image_map{ hdr "probe_bathroom_FINAL.hdr"
      gamma 1.1
      map_type 1 interpolate 2
      }
    }
    rotate <0,60,0> //
  }
#else
  background { color White }
#end


camera { orthographic location <0, 0, -40> look_at <0, 0, 0> }

#include "clock_body.inc"
#include "hands.inc"

#local watch_body = 
union {
  object {watch_frame}
  object {winding_ext_1}
  // object {backside_cover}
  object {cover_glass}
  object {fixing_ring}
  object {ring_on_ring}
  object { main_body texture { T_Body_Texture } }
  object { fixing_parts_1 texture { T_Body_Texture } }
  object { fixing_parts_2 texture { T_Body_Texture } }
  object { fixing_parts_3 texture { T_Body_Texture } }
  object { overlay scale <1.01, 1.01, 1> translate <0, 0, -0.09> texture { T_Body_Texture } normal { T_Engraved_Normals scale 40 } }
  /* for pinion */
  object { fixing_parts_4 texture { T_Body_Texture } rotate z*-15 translate <9.5, -7.5, -2.5> }

  object { number_plate }
  #local total_radius = 42/2;
  object { screw(2, 0.4, 3) translate <0, total_radius * 0.88, 0> rotate z*-139 translate <0, 0, -5.5> }
  object { screw(2, 0.4, 3) translate <0, total_radius * 0.88, 0> rotate z*-50 translate <0, 0, -5.5> }
  object { screw(2, 0.4, 3) translate <0, total_radius * 0.88, 0> rotate z*30 translate <0, 0, -5.5> }
  object { screw(2, 0.4, 3) translate <0, total_radius * 0.88, 0> rotate z*100 translate <0, 0, -5.5> }
  /* clock hands */
  object { minutes_hand rotate x*-90 rotate z*12 translate <0, 0, 0.82> }
  object { hours_hand rotate x*-90 rotate z*(12*3) translate <0, 0, 0.72> }
  object { seconds_hand scale 1.4 rotate x*-90 rotate z*90 translate <0, -12, 0.70> }
}

#local watch_gears =
  union {
    /* barrel gear */
    object { gear_barrel(19/2, 18/2, 0.3, 120, 1.6) translate <-10.5, 4, -3> }
    /* central gear */
    object { gear_5(14.8/2, 13.6/2, 0.3, 8, 0.65, 0.15, 0.3, 6, 120, 1) rotate <180, 0, 0> translate <0, 0, -4.2> }
    /* gears on front panel */
    object { gear_4(4.0/2, 3.8/2, 0.2, 40) translate <0, 0, -1.4> }
    object { gear_4(9/2, 8.8/2, 0.2, 80) translate <5, 4, -1.4> }
    object { gear_4(4.8/2, 4.5/2, 0.2, 40) translate <5, 4, -1.0> }
    object { gear_4(3.8/2, 3.5/2, 0.2, 40) translate <0, 8, -1.4> }
    object { gear_4(8.0/2, 7.8/2, 0.2, 80) translate <0, 0, -1.0> } // for hours hand
    /* join */
    object { gear_5(12/2, 11/2, 0.1, 4, 0.5, 0.5, 0.1, 3, 120, 1) translate <-5, -7, -3.5> }
    /* under central wheel */
    object { gear_5(12/2, 11/2, 0.1, 8, 0.2, 0.55, 0.25, 6, 120, 1) rotate <0, 0, 0> translate <0, -12, -3.1> }
    /* escape wheel */
    object { gear_escape_wheel(7/2, 6.5/2, 0.1, 4, 0.5, 0.5, 0.1, 4, 20, 1) rotate <0, 0, 0> translate <7, -12, -2> }
    /* pinion */
    object { gear_pinion(10/2, 0, 0, 4, 0.2, 0.2, 0.1) rotate z*-15 translate <9.5, -7.5, -2> }

    /* balance gear/hairspring*/
    object { gear_balance(15/2, 14/2, 0.3, 4, 0.5, 0.5, 0.1, 3) rotate <0, 0, 0> translate <12, -2, -3> }
    object { balance_wheel_supports(5.5, 0.2) rotate z*55 translate <12, -2, -6 - 0.2> }
    /* balance spring to fixing point */
    #local trans1 = transform { rotate <0, 0, 19> };
    #local trans1_1 = transform { rotate <0, 0, 55> };
    #local vert_vec = vtransform(<-4.8, 0, 0>, trans1);
    #local vert_vec = vtransform(vert_vec, trans1_1);
    #ifdef (last_spring_pos)
      #local trans2 = transform { rotate <0, 0, 0> };
      #local vert_vec_1 = vtransform(<last_spring_pos.x, last_spring_pos.y, 0>, trans2);
      cylinder { <vert_vec_1.x, vert_vec_1.y, last_spring_pos.z>, <vert_vec.x, vert_vec.y, last_spring_pos.z>, 0.05 texture { T_Gear_Texture } translate <12, -2, -3> }
  #end

#if (1)
    /* gears for winding */
    object { gear_4(14.8/2, 14.0/2, 0.4, 100) translate <-10.5, 4, -5.6> }
    object { screw(2, 0.4, 3) translate <-10.5, 4, -5.8> }
    object { gear_4(8.8/2, 8.0/2, 0.4, 80) translate <0, 10, -5.6> }
    object { screw(1.5, 0.4, 3) translate <0, 10, -5.8> }
    /* winding click */
    object { gear_winding_click(3/2, 1/2, 0.4, 20) rotate <180, 0, 0> translate <-12.5, -5, -5.6> }
    object { screw(1.5, 0.4, 2) translate <-12.5, -5, -5.8> }

    /* cylinder gears on winding axis */
    object { gear_cylider(3.3/2, 2, 50) rotate x*-90 translate <0, 11.9, -3> }
    object { gear_cylider(3.3/2, 2, 25) rotate x*90  translate <0, 10.5, -3> }

    object { gear_cylider(3.3/2, 2, 25) rotate x*-90  translate <0, 15.9, -3> }
    object { gear_4(4.7/2, 4.5/2, 0.5, 40) rotate x*-90  translate <0, 14.6, -3>  }
#end
  }


#if (1)
  camera { location <0, 150, -200> look_at 0 angle 35
    aperture 0.1
    blur_samples 12
    focal_point <1, 1, -2>
    confidence 0.9

  }
  /* first watch */
  #local bkside_center = (max_extent(backside_cover) - min_extent(backside_cover))/2;
  #local bkside = object { backside_cover translate bkside_center rotate x*-90 };

  #local wb_center = (max_extent(watch_body) - min_extent(watch_body))/2;
  #local wb0 = object { watch_body translate wb_center };
  #local wb1 = object { wb0 rotate x*-20 };

  #local w1 =
  union {
    object { wb1 translate y*-10 }
    object { bkside translate z*max_extent(bkside).z }
    rotate y*132
  }

  object {
    object { w1 translate y*-min_extent(w1).y}
    translate <-15, 0, 50>
  }

  /* second watch */
  #local w2 =
  union {
    object {watch_frame}
    object {winding_ext_1}
    // object {backside_cover}
    // object {cover_glass}
    object {fixing_ring}
    object {ring_on_ring}
    object { main_body texture { T_Body_Texture } }
    object { fixing_parts_1 texture { T_Body_Texture } normal { T_Engraved_Normals scale 40 } }
    object { fixing_parts_2 texture { T_Body_Texture } normal { T_Engraved_Normals scale 40 } }
    object { fixing_parts_3 texture { T_Body_Texture } normal { T_Engraved_Normals scale 40 } }
    object { number_plate }
    #local total_radius = 42/2;
    object { screw(2, 0.4, 3) translate <0, total_radius * 0.9, 0> rotate z*-139 translate <0, 0, -5.5> }
    object { screw(2, 0.4, 3) translate <0, total_radius * 0.9, 0> rotate z*-50 translate <0, 0, -5.5> }
    object { screw(2, 0.4, 3) translate <0, total_radius * 0.9, 0> rotate z*30 translate <0, 0, -5.5> }
    object { screw(2, 0.4, 3) translate <0, total_radius * 0.9, 0> rotate z*100 translate <0, 0, -5.5> }

    object {watch_gears}
    rotate x*90
  }

  object {
    object {w2}
    translate <45, -min_extent(w2).y, -30>
    rotate y*47
  }

/* table */
  #declare PlankNormal =
    normal {
      gradient x 2 slope_map { [0 <0,1>][.05 <1,0>][.95 <1,0>][1 <0,-1>] }
      scale 2
    };

  #local pl1 = plane {
    y, -.25
    texture { T_Wood4 }
    normal
    { average normal_map
      { [1 PlankNormal]
        [1 wood .5 slope_map { [0 <0,0>][.5 <.5,1>][1 <1,0>] }
           turbulence .5 scale <1, 1, 10>*.5]
      }
      rotate y*20
    }
    finish { specular .5 reflection .2 }
    scale <40, 2, 40>
    translate <-10, 0, -30>
    rotate y*47
  }

  box {<-100, -5, -90>, <100, 0, 90> texture { T_Wood4 } finish { specular .5 reflection .2 }
    scale <20, 2, 20>
    translate <-10, 0, -30>
    rotate y*63
  }
#end


