#include "glass.inc"

/* lots of scavanged code */

#declare p_micro2=
pigment{
 crackle turbulence 1
 color_map{
  [0.00 White*.7]
  [0.01 White*.7]
  [0.02 White]
  [1.00 White]
 }
 scale 5
}
#declare p_micro1=
pigment{
 cells 
 turbulence .5
 color_map{
  [0.0 White*.9]
  [1.0 White]
 }
 scale .01
}
// + average all the normals
#declare p_brushed_new=
pigment{
 average turbulence 0
 pigment_map{
  [0.0 p_micro1]
  [0.1 p_micro1 rotate 45*x]
  [0.2 p_micro1 rotate -45*x]
  [0.3 p_micro2]
  [0.4 p_micro1 rotate 45*y]
  [0.5 p_micro1 rotate -45*y]
  [0.6 p_micro1 rotate 90*z]
  [0.7 p_micro2 rotate 45]
  [0.8 p_micro1 rotate 45*z]
  [0.9 p_micro1 rotate 90*y]
  [1.0 p_micro1 rotate -45*z]
 }
 scale .1
}

// + declare the final normal as image_pattern
#declare n_brushed_new=
normal{pigment_pattern{p_brushed_new} .4}

#local S = seed(019283);
#local brass_base = <201/255, 205/255, 167/255>;
#local P_Brass = rgb brass_base;
#local A_Brass = P_Brass * 0.12 + <0.1,0.1,0.1>;
#local R_Brass = P_Brass * 0.30 + <0.25,0.25,0.25>;
#local D_Brass = 1 - (((R_Brass.red+R_Brass.green+R_Brass.blue)/3)
                       + ((A_Brass.red+A_Brass.green+A_Brass.blue)/3));
#declare T_Gear_Texture = texture {
  pigment { color P_Brass }
#if (use_fancy_textures = 1)
  /* finish A */
  finish {
    brilliance 2
    diffuse D_Brass
    ambient A_Brass * 0.3
    reflection R_Brass
    metallic 1
    specular 0.20
    roughness 1/20
  }
  /* finish B */
  normal {
    n_brushed_new
  }
#end
};

#declare T_Axis_Texture = texture { T_Chrome_2D };

#local brass_base = <216/255, 221/255, 185/255>;
#local P_Brass = rgb brass_base;
#local A_Brass = P_Brass * 0.12 + <0.1,0.1,0.1>;
#local R_Brass = P_Brass * 0.30 + <0.25,0.25,0.25>;
#local D_Brass = 1 - (((R_Brass.red+R_Brass.green+R_Brass.blue)/3)
                       + ((A_Brass.red+A_Brass.green+A_Brass.blue)/3));

#declare T_Body_Texture = texture { /*T_Brass_5A*/
#if (use_fancy_textures = 1)  
  pigment { color P_Brass }

  finish {
    brilliance 2
    diffuse D_Brass
    ambient A_Brass * 0.3
    reflection R_Brass * 0.7
    metallic 1
    specular 0.20
    roughness 1/20
  }

  normal {
    n_brushed_new
  }
#else
  pigment { Red }
#end
};

#local S = seed(019283);
#local frame_base = <46/255, 46/255, 46/255>;
#local P_Frame_Metal = rgb frame_base;
#local A_Frame_Metal = P_Frame_Metal * 0.12 + <0.1,0.1,0.1>;
#local R_Frame_Metal = P_Frame_Metal * 0.30 + <0.25,0.25,0.25>;
#local D_Frame_Metal = 1 - (((R_Frame_Metal.red+R_Frame_Metal.green+R_Frame_Metal.blue)/3)
                       + ((A_Frame_Metal.red+A_Frame_Metal.green+A_Frame_Metal.blue)/3));
#declare T_Frame_Texture = texture { /*T_Gold_5A*/ /*T_Brass_4A*/
  pigment { color P_Frame_Metal }
#if (use_fancy_textures = 1)
  /* finish A */
  finish {
    brilliance 1
    diffuse D_Frame_Metal
    ambient A_Frame_Metal * 0.1
    reflection R_Frame_Metal * 0.03
    metallic 0.2
    specular 0.10
    roughness 1/40
  }
  /* finish B */
  normal {
    n_brushed_new
  }
#end
};

// Glass Finishes
#declare F_Glass1 =
finish {
  specular 1
  roughness 0.001
  ambient 0
  diffuse 0
  reflection {
    0.2, 1.0
    fresnel on
  }
  conserve_energy
}

#declare T_Glass1 =
texture {
  pigment { Col_Glass_General }
  finish { F_Glass6 }
}

/* no idea */
#declare F_Emalge =
finish {
  ambient 0.6
  diffuse 0.8
}   

#declare T_Number_Plate_Texture =
texture {
  pigment { image_map { png "number_plate.png" interpolate 2 once } }
  translate <-0.5, -0.5, -0.5>
  finish  { F_Emalge }
}

#declare T_Seconds_Plate_Texture =
texture {
  pigment { image_map { png "seconds_plate.png" interpolate 2  once } }
  translate <-0.5, -0.5, -0.5>
  normal {wrinkles 0.2 turbulence 0.05 }
}

#declare T_Engraved_Normals =
normal {
  bump_map {
    png "normals.png" interpolate 2 once 
    map_type 0
    bump_size 5.0
  }
  translate <-0.5, -0.5, -0.5>
}
